#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <exception>
#include <LuceneHeaders.h>
#include <ConcurrentMergeScheduler.h>
#include <FileUtils.h>
#include <MiscUtils.h>

extern "C" 
{
    #include <string.h>
    #include <sys/time.h>
}

#define NUM_THREADS 1

using namespace std;
using namespace Lucene;

void parallel_index(int tid, int num_threads, IndexWriterPtr iwriter, 
        vector<DocumentPtr> documents)
{
    for (tid; tid < documents.size(); tid += num_threads) {
        iwriter->addDocument(documents[tid]);
    }
}

int main(int argc, char **argv)
{
    AnalyzerPtr analyzer;
    RAMDirectoryPtr directory;
    IndexWriterPtr iwriter;
    IndexReaderPtr ireader;
    IndexSearcherPtr isearcher;
    QueryParserPtr parser;
    ifstream in;
    vector<string> inputFiles;
    vector<DocumentPtr> documents;
    struct timeval start, end;
    long indexTime, indexSize, searchTime;
    ConcurrentMergeSchedulerPtr scheduler;
    vector<thread> threads;
    
    indexTime = 0;
    indexSize = 0;
    searchTime = 0;
    try {
        // read the file paths from the input file
        inputFiles = vector<string>();
        in.open(argv[1], ifstream::in);
        while(in.good()) {
            char cline[1024];
            in.getline(cline, 1024);
            if (strlen(cline) <= 0) {
                break;
            }
            inputFiles.push_back(string(cline));
        }
        in.close();

        // create a list of document that are going to be indexed
        documents = vector<DocumentPtr>();
        for (auto inputFile : inputFiles) {
            DocumentPtr document = newLucene<Document>();
            char filename[1024];
            
            strcpy(filename, inputFile.c_str());
            String sourceFile(StringUtils::toUnicode(filename));

            document->add(newLucene<Field>(L"content",
                    newLucene<FileReader>(sourceFile)));
            document->add(newLucene<Field>(L"filepath", sourceFile,
                    Field::STORE_YES, Field::INDEX_NOT_ANALYZED));

            documents.push_back(document);
        }

        // use the standard text analyzer
        analyzer = newLucene<StandardAnalyzer>(LuceneVersion::LUCENE_CURRENT);
        
        // store the index in main memory (RAM)
        directory = newLucene<RAMDirectory>();
        
        // create and index writer
        iwriter = newLucene<IndexWriter>(directory, analyzer, true,
                IndexWriter::MaxFieldLengthUNLIMITED);

        scheduler = boost::static_pointer_cast<ConcurrentMergeScheduler>(
                iwriter->getMergeScheduler());
        scheduler->setMaxThreadCount(NUM_THREADS);
        iwriter->setMergeScheduler(scheduler);
        
        // index files in parallel using multiple threads
        gettimeofday(&start, NULL);
        for (int tid = 0; tid < NUM_THREADS; tid++) {
            threads.push_back(thread(parallel_index, tid, NUM_THREADS, 
                    iwriter, documents));
        }
        for (auto& th : threads) {
            th.join();
        }
        iwriter->commit();
        gettimeofday(&end, NULL);
        
        // calculate the time taken to index the files
        indexTime += (((long) end.tv_sec - (long) start.tv_sec) 
                * 1000000 + (end.tv_usec - start.tv_usec)) / 1000;

        // get the total size of the index
        indexSize = directory->sizeInBytes() / 1000;
        
        iwriter->close();

        // create an index reader
        ireader = IndexReader::open(directory);
        isearcher = newLucene<IndexSearcher>(ireader);
        parser = newLucene<QueryParser>(LuceneVersion::LUCENE_CURRENT,
                L"content", analyzer);

        // read the terms from the second input file and search the index
        in.open(argv[2], ifstream::in);
        gettimeofday(&start, NULL);
        while(in.good()) {
            char word[1024];

            in.getline(word, 1024);
            if (strlen(word) <= 0) {
                break;
            }
            
            String term(StringUtils::toUnicode(word));
            QueryPtr query = parser->parse(term);
            
            Collection<ScoreDocPtr> hits = isearcher->search(
                    query, 1000)->scoreDocs;
        }
        in.close();
        gettimeofday(&end, NULL);

        isearcher->close();
        directory->close();
        
        // calculate the time it took to search all the terms
        searchTime += (((long) end.tv_sec - (long) start.tv_sec) 
                * 1000000 + (end.tv_usec - start.tv_usec)) / 1000;
    } catch(LuceneException& e) {
        wcout << e.getError() << endl;
    } catch(std::runtime_error& e) {
        cout << e.what() << endl;
    }

    cout << "IndexTime: " << indexTime << " ms" << endl;
    cout << "IndexSize: " << indexSize << " kB" << endl;
    cout << "SearchTime: " << searchTime << " ms" << endl;

    return 0;
}
